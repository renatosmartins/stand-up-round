import AppContext from "./core/AppContext";
import ManagePeople from "./manage-people";
import ShufflePeople from "./shuffle-people";
import Controls from "./shuffle-people/Controls";

const App = () => (
  <AppContext>
    <Controls />
    <ShufflePeople />
    <ManagePeople />
  </AppContext>
);

export default App;
