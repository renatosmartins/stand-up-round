import { fireEvent, render, screen } from "@testing-library/react";
import { FC } from "react";
import AppContext, { useAppContext } from "./AppContext";
import { Person } from "./people";

const samplePerson = (
  i: number,
  options: { hidden?: boolean; done?: boolean } = {}
): Person => ({
  id: i,
  name: `P${i}`,
  ...options,
});

jest.mock("./people");

describe("AppContext", () => {
  it("allows children to access and modify its context", () => {
    const Component: FC = () => {
      const {
        people,
        setPeople,
        visiblePeople,
        setVisiblePeople,
        selectedPerson,
        setSelectedPerson,
        isManageDialogVisible,
        setManageDialogVisible,
      } = useAppContext();

      const modifyContext = () => {
        setVisiblePeople((previous) =>
          previous.map((p, i) => ({ ...p, done: p.id === 2 }))
        );
        setPeople((previous) => [
          previous[1],
          samplePerson(3),
          samplePerson(4, { hidden: true }),
        ]);
        setSelectedPerson(samplePerson(3));
        setManageDialogVisible(true);
      };

      return (
        <>
          <button onClick={modifyContext} />
          <ul>
            <li>People: {people.map((p) => p.name).join(", ")}</li>
            <li>
              Visible people: {visiblePeople.map((p) => p.name).join(", ")}
            </li>
            <li>
              Done:{" "}
              {visiblePeople
                .filter((p) => p.done)
                .map((p) => p.name)
                .join(", ")}
            </li>
            <li>Selected person: {selectedPerson?.name || "-"}</li>
            <li>ManageDialog visible: {String(isManageDialogVisible)}</li>
          </ul>
        </>
      );
    };

    render(
      <AppContext>
        <Component />
      </AppContext>
    );

    const items = screen.getAllByRole("listitem");
    expect(items).toHaveLength(5);
    expect(items[0]).toHaveTextContent(/^People: P1, P2, P3$/);
    expect(items[1]).toHaveTextContent(/^Visible people: P1, P2$/);
    expect(items[2]).toHaveTextContent(/^Done:$/);
    expect(items[3]).toHaveTextContent(/^Selected person: -$/);
    expect(items[4]).toHaveTextContent(/^ManageDialog visible: false$/);

    fireEvent.click(screen.getByRole("button"));
    expect(items[0]).toHaveTextContent(/^People: P2, P3, P4$/);
    expect(items[1]).toHaveTextContent(/^Visible people: P2, P3$/);
    expect(items[2]).toHaveTextContent(/^Done: P2$/);
    expect(items[3]).toHaveTextContent(/^Selected person: P3$/);
    expect(items[4]).toHaveTextContent(/^ManageDialog visible: true$/);
  });
});
