import {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import {
  allPeople,
  Person,
  PersonWithAngles,
  savePeople,
  personWithAngles,
} from "./people";

interface AppContextProps {
  selectedPerson?: Person;
  setSelectedPerson: Dispatch<SetStateAction<Person | undefined>>;
  people: Person[];
  setPeople: Dispatch<SetStateAction<Person[]>>;
  visiblePeople: PersonWithAngles[];
  setVisiblePeople: Dispatch<SetStateAction<PersonWithAngles[]>>;
  isManageDialogVisible: boolean;
  setManageDialogVisible: Dispatch<SetStateAction<boolean>>;
}

const Context = createContext<AppContextProps | undefined>(undefined);

const getVisiblePeople = (people: Person[]): PersonWithAngles[] =>
  people.filter((p) => !p.hidden).map(personWithAngles);

const AppContext = ({ children }: PropsWithChildren<{}>) => {
  const [people, setPeople] = useState<Person[]>(allPeople);
  const [selectedPerson, setSelectedPerson] = useState<Person>();
  const [isManageDialogVisible, setManageDialogVisible] = useState(
    !people.length
  );
  const [visiblePeople, setVisiblePeople] = useState<PersonWithAngles[]>(
    getVisiblePeople(people)
  );

  const contextValue = useMemo(
    () => ({
      selectedPerson,
      setSelectedPerson,
      people,
      setPeople,
      visiblePeople,
      setVisiblePeople,
      isManageDialogVisible,
      setManageDialogVisible,
    }),
    [selectedPerson, people, visiblePeople, isManageDialogVisible]
  );

  useEffect(() => {
    setVisiblePeople((oldVisiblePeople) => {
      const visiblePeopleMap = new Map(oldVisiblePeople.map((p) => [p.id, p]));
      const newVisiblePeople = getVisiblePeople(
        people.map((p) => ({ ...p, done: visiblePeopleMap.get(p.id)?.done }))
      );
      return newVisiblePeople;
    });
  }, [people]);

  useEffect(() => {
    savePeople(people);
  }, [people]);

  return <Context.Provider value={contextValue}>{children}</Context.Provider>;
};

export default AppContext;

export const useAppContext = () => {
  return useContext(Context) as AppContextProps;
};
