import { render, screen } from "@testing-library/react";
import Avatar from "./Avatar";

describe("<Avatar>", () => {
  const mockPerson = {
    id: 1,
    name: "Person Name",
    angle: 0,
    hue: 0,
    initials: "PN",
  };

  it("renders", () => {
    render(<Avatar size={2} person={mockPerson} />);
    const avatar = screen.getByRole("figure");
    expect(avatar).toHaveTextContent(mockPerson.initials);
    expect(avatar).toHaveAttribute("title", mockPerson.name);
    expect(avatar).not.toHaveStyleRule("opacity");
  });

  it("renders selected person", () => {
    render(<Avatar size={2} person={{ ...mockPerson, done: true }} />);
    expect(screen.getByRole("figure")).toHaveStyleRule("opacity", "0.4");
  });
});
