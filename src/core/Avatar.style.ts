import styled from "styled-components";
import { getColor } from "../core/colors";
import { AvatarProps } from "./Avatar";

const StyledAvatarBase = styled.div<Pick<AvatarProps, "size">>({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: "50%",
  boxSizing: "border-box",
  fontFamily:
    "'Courier New', source-code-pro, Menlo, Monaco, Consolas, monospace",
  fontWeight: 600,
  color: "white",
  textShadow: "0 0 6px black",
  cursor: "default",
});

export const StyledAvatar = styled(StyledAvatarBase)<
  Pick<AvatarProps, "person" | "size">
>(({ person: { avatar, hue, done }, size }) => ({
  // gradient between main color and a neighbour (shift hue by 10%) semi-transparent color
  background: `linear-gradient(45deg, ${getColor(hue)} 0%, ${getColor(
    hue + 360 / 10,
    0.6
  )} 100%)`,

  border: `${size / 24}vmin solid #222`,
  height: `${size}vmin`,
  width: `${size}vmin`,
  fontSize: `${size / 2}vmin`,

  ...(avatar && {
    backgroundImage: `url(${avatar})`,
    // to avoid rendering issues showing the border of the image
    backgroundSize: "calc(100% + 2px)",
    backgroundPosition: "-1px -1px",
  }),

  ...(done && { opacity: 0.4 }),
}));
