import { getPersonInitials, PersonWithAngles } from "./people";
import { StyledAvatar } from "./Avatar.style";

export interface AvatarProps {
  person: PersonWithAngles;
  size: number;
}

const Avatar = ({ person, size, ...rest }: AvatarProps) => (
  <StyledAvatar
    role="figure"
    person={person}
    size={size}
    title={person.name}
    {...rest}
  >
    {!person.avatar && getPersonInitials(person.name)}
  </StyledAvatar>
);

export default Avatar;
