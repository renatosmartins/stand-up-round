import {
  ChangeEvent,
  KeyboardEvent,
  MouseEvent,
  useCallback,
  useRef,
} from "react";
import { useAppContext } from "./AppContext";
import { AvatarInputProps } from "./AvatarInput";
import { Person, personWithAngles } from "./people";

export const useAvatarInput = (
  person: Person,
  index: number,
  onImage: AvatarInputProps["onImage"]
) => {
  const { people } = useAppContext();
  const inputRef = useRef<HTMLInputElement>(null);

  const onLabelKeyDown = useCallback(
    (e: KeyboardEvent) => {
      if (e.key !== "Enter") {
        return;
      }
      person.avatar ? onImage(undefined) : inputRef.current?.click();
    },
    [person.avatar, onImage]
  );

  const removeAvatar = useCallback(
    (e: MouseEvent) => {
      e.preventDefault();
      onImage(undefined);
    },
    [onImage]
  );

  const onInputChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const files = e.target.files || [];
      if (!files.length) {
        return;
      }

      const reader = new FileReader();
      reader.onload = () => {
        onImage(reader.result?.toString());
      };
      reader.readAsDataURL(files[0]);
    },
    [onImage]
  );

  return {
    personWithAngles: personWithAngles(person, index, people),
    labelProps: {
      onKeyDown: onLabelKeyDown,
      tabIndex: 0,
      onClick: person.avatar ? removeAvatar : undefined,
    },
    inputProps: {
      ref: inputRef,
      onChange: onInputChange,
      tabIndex: -1,
    },
  };
};
