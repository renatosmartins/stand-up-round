import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { useAppContext } from "./AppContext";
import AvatarInput from "./AvatarInput";

jest.mock("./AppContext");

const useAppContextMock = useAppContext as jest.MockedFunction<
  typeof useAppContext
>;

describe("<AvatarInput>", () => {
  const onImage = jest.fn();
  const mockPerson = {
    id: 1,
    name: "Person 1",
  };

  beforeEach(() => {
    useAppContextMock.mockReturnValue({
      people: [],
      setPeople: jest.fn(),
      visiblePeople: [],
      setVisiblePeople: jest.fn(),
      setSelectedPerson: jest.fn(),
      isManageDialogVisible: false,
      setManageDialogVisible: jest.fn(),
    });
  });
  afterEach(jest.clearAllMocks);

  const setup = ({ avatar }: { avatar?: string }) =>
    render(
      <AvatarInput
        person={{ ...mockPerson, avatar }}
        index={0}
        onImage={onImage}
      />
    );

  it("renders a person initials", () => {
    setup({});
    const avatar = screen.getByRole("figure");
    expect(avatar).toHaveTextContent("P1");
    expect(avatar).not.toHaveStyleRule("background-image");
  });

  it("renders a person avatar", () => {
    setup({ avatar: "avatar-url" });
    const avatar = screen.getByRole("figure");
    expect(avatar).not.toHaveTextContent("P1");
    expect(avatar).toHaveStyleRule("background-image", "url(avatar-url)");
  });

  it("triggers callback on image uploaded with its base64 content", async () => {
    setup({});
    expect(onImage).not.toHaveBeenCalled();
    userEvent.upload(
      screen.getByLabelText("upload avatar image"),
      new File([":)"], "avatar.png")
    );
    await waitFor(() => {
      expect(onImage).toHaveBeenCalledTimes(1);
    });
    expect(onImage).toHaveBeenCalledWith(
      "data:application/octet-stream;base64,Oik="
    );

    userEvent.upload(screen.getByLabelText("upload avatar image"), []);
    expect(onImage).not.toHaveBeenCalledTimes(2);
  });

  it("removes avatar with a click", () => {
    setup({ avatar: "avatar-url" });
    expect(onImage).not.toHaveBeenCalled();
    fireEvent.click(screen.getByRole("figure"));
    expect(onImage).toHaveBeenCalledWith(undefined);
  });

  it("removes avatar with the keyboard", () => {
    setup({ avatar: "avatar-url" });
    expect(onImage).not.toHaveBeenCalled();
    fireEvent.keyDown(screen.getByRole("figure"), { key: "A" });
    expect(onImage).not.toHaveBeenCalled();
    fireEvent.keyDown(screen.getByRole("figure"), { key: "Enter" });
    expect(onImage).toHaveBeenCalledWith(undefined);
  });
});
