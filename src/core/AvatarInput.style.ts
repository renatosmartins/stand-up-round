import styled from "styled-components";
import Avatar from "./Avatar";
import { Person } from "./people";

export const StyledLabel = styled.label<{ person: Person }>(({ person }) => ({
  display: "flex",
  position: "relative",
  borderRadius: "50%",

  ...(person.avatar && {
    "&:hover::after, &:focus-visible::after": {
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      borderRadius: "50%",
      backdropFilter: "blur(1px)",
      cursor: "pointer",
      content: "'✕'",
      background: "rgba(255, 255, 255, 0.1)",
      color: "white",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "0.8rem",
      fontWeight: "bold",
      textShadow: "0 1px 2px black",
    },
  }),
}));

export const StyledInput = styled.input({
  position: "absolute",
  height: 1,
  width: 1,
  opacity: 0,
  overflow: "hidden",
  cursor: "pointer",
});

export const StyledAvatar = styled(Avatar)({
  height: 26,
  width: 26,
  border: "1px solid rgba(0, 0, 0, 0.8)",
  fontSize: "0.8rem",
  cursor: "pointer",
});
