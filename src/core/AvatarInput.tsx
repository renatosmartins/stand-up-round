import { useAvatarInput } from "./AvatarInput.hook";
import { StyledAvatar, StyledInput, StyledLabel } from "./AvatarInput.style";
import { Person } from "./people";

export interface AvatarInputProps {
  person: Person;
  index: number;
  onImage: (imageUrl?: string) => void;
}

const AvatarInput = ({ person, index, onImage }: AvatarInputProps) => {
  const { personWithAngles, labelProps, inputProps } = useAvatarInput(
    person,
    index,
    onImage
  );
  return (
    <>
      <StyledLabel
        person={personWithAngles}
        htmlFor={`avatar-upload-${index}`}
        {...labelProps}
      >
        <StyledAvatar person={personWithAngles} size={4} />
      </StyledLabel>
      <StyledInput
        id={`avatar-upload-${index}`}
        type="file"
        accept="image/*"
        aria-label="upload avatar image"
        {...inputProps}
      />
    </>
  );
};

export default AvatarInput;
