import { HTMLAttributes, PropsWithChildren } from "react";
import styled from "styled-components";

export type ButtonProps = HTMLAttributes<HTMLButtonElement> &
  PropsWithChildren<{
    onClick?: () => void;
  }>;

const StyledButton = styled.button({
  padding: "4px 8px",
  border: "2px solid",
  borderRadius: "4px",
  minWidth: "42px",
  background: "floralwhite",
  fontWeight: "bold",
  cursor: "pointer",
});

const Button = ({ onClick, children, ...rest }: ButtonProps) => (
  <StyledButton onClick={onClick} {...rest}>
    {children}
  </StyledButton>
);

export default Button;
