import styled from "styled-components";

const ButtonSmall = styled.button({
  display: "inline-flex",
  alignItems: "center",
  justifyContent: "center",
  height: "1rem",
  width: "1rem",
  padding: "0.2rem",
  lineHeight: "1rem",
  border: "none",
  background: "none",
  cursor: "pointer",
  borderRadius: 4,

  "&:hover, &:focus": {
    background: "rgba(255, 255, 255, 0.4)",
  },
});

export default ButtonSmall;
