import { KeyboardEventHandler, useCallback, useEffect, useRef } from "react";
import { DialogProps } from "./Dialog";

export const useDialog = ({ onClose }: DialogProps) => {
  const dialogRef = useRef<HTMLDivElement>(null);

  const onEscape = useCallback<KeyboardEventHandler>(
    (e) => {
      if (e.key === "Escape" && onClose) {
        onClose();
      }
    },
    [onClose]
  );

  // capture focus
  useEffect(() => {
    if (dialogRef.current) {
      dialogRef.current.focus();
    }
  }, []);

  return {
    dialogProps: { ref: dialogRef, onKeyDown: onEscape },
  };
};
