import { fireEvent, render, screen } from "@testing-library/react";
import Dialog, { DialogProps } from "./Dialog";

describe("<Dialog>", () => {
  const setup = ({ visible = true, ...rest }: Partial<DialogProps> = {}) => {
    const utils = render(<Dialog visible={visible} {...rest} />);
    const dialog = screen.queryByRole("dialog");
    const close = screen.queryByRole("button");
    const overlay = screen.queryByRole("none");
    return { ...utils, dialog, close, overlay };
  };

  it("does not render", () => {
    const { container } = setup({ visible: false });
    expect(container).toBeEmptyDOMElement();
  });

  it("renders", () => {
    const textContent = "dialog content";
    const { container, dialog } = setup({ children: textContent });
    expect(container).not.toBeEmptyDOMElement();
    expect(dialog).toHaveTextContent(textContent);
  });

  it("closes when close button is clicked", () => {
    const onClose = jest.fn();
    const { close } = setup({ onClose });
    expect(onClose).toHaveBeenCalledTimes(0);
    fireEvent.click(close!);
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("closes when overlay is clicked", () => {
    const onClose = jest.fn();
    const { overlay } = setup({ onClose });
    expect(onClose).toHaveBeenCalledTimes(0);
    fireEvent.click(overlay!);
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("closes on escape key pressed", () => {
    const onClose = jest.fn();
    const { dialog } = setup({ onClose });
    expect(onClose).toHaveBeenCalledTimes(0);
    fireEvent.keyDown(dialog!, { key: "Enter" });
    expect(onClose).toHaveBeenCalledTimes(0);
    fireEvent.keyDown(dialog!, { key: "Escape" });
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("sets focus on dialog", () => {
    const { dialog } = setup();
    expect(dialog).toHaveFocus();
  });
});
