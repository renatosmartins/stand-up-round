import styled from "styled-components";

const maxHeight = "60vh";

export const StyledOverlay = styled.div({
  position: "fixed",
  height: "100%",
  width: "100%",
  background: "rgba(255, 255, 255, 0.15)",
  cursor: "pointer",
  backdropFilter: "blur(8px)",
});

export const StyledDialog = styled.div({
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  borderRadius: "8px",
  boxShadow: "0 10px 50px -30px",
  boxSizing: "border-box",
  zIndex: 100,
  border: "3px solid black",
  background:
    "#fff8c9 linear-gradient(142deg, rgba(245, 135, 255, 0.6), #fff8c9)",
  outline: "none",
});

export const StyledContainer = styled.div({
  maxHeight: maxHeight,
  maxWidth: "80vw",
  display: "flex",
  padding: "2rem",
  overflow: "hidden",
  boxSizing: "border-box",
});

export const StyledScrollable = styled.div({
  overflow: "auto",
  padding: "1rem",
});

export const StyledClose = styled.button({
  position: "absolute",
  right: 0,
  top: 0,
  border: 0,
  height: "24px",
  width: "24px",
  margin: "4px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: "50%",
  background: "none",
  fontSize: "1rem",
  color: "rgba(0, 0, 0, 0.3)",
  boxSizing: "border-box",
  cursor: "pointer",
  outline: "none",

  "&:hover, &:focus-visible": {
    background: "rgba(0, 0, 0, 0.1)",
    color: "rgba(0, 0, 0, 0.6)",
  },
});
