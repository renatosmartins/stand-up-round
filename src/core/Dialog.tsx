import { PropsWithChildren } from "react";
import { useDialog } from "./Dialog.hook";
import {
  StyledClose,
  StyledContainer,
  StyledDialog,
  StyledOverlay,
  StyledScrollable,
} from "./Dialog.style";

export type DialogProps = PropsWithChildren<{
  visible: boolean;
  onClose?: () => void;
}>;

const Dialog = (props: DialogProps) => {
  const { dialogProps } = useDialog(props);
  return props.visible ? (
    <>
      <StyledOverlay onClick={props.onClose} role="none" />
      <StyledDialog {...dialogProps} role="dialog" tabIndex={0}>
        <StyledContainer>
          <StyledScrollable>{props.children}</StyledScrollable>
        </StyledContainer>
        <StyledClose onClick={props.onClose}>✖️</StyledClose>
      </StyledDialog>
    </>
  ) : null;
};

export default Dialog;
