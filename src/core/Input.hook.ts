import {
  ChangeEventHandler,
  KeyboardEventHandler,
  useCallback,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import { InputProps } from "./Input";

export const useInput = ({ initialValue, onCommit }: InputProps) => {
  const [value, setValue] = useState(initialValue);
  const [width, setWidth] = useState(0);
  const sizerRef = useRef<HTMLSpanElement>(null);
  const inputRef = useRef<HTMLInputElement>(null);

  useLayoutEffect(() => {
    setWidth(sizerRef.current?.offsetWidth || 0);
  }, [value]);

  const onChange = useCallback<ChangeEventHandler<HTMLInputElement>>((e) => {
    setValue(e.target.value);
  }, []);

  const onKeyDown = useCallback<KeyboardEventHandler<HTMLInputElement>>(
    (e) => e.key === "Enter" && inputRef.current?.blur(),
    []
  );

  const onBlur = useCallback(() => onCommit(value), [value, onCommit]);

  return {
    sizerProps: { ref: sizerRef, children: value },
    inputProps: {
      type: "text",
      ref: inputRef,
      value,
      width,
      onChange,
      onKeyDown,
      onBlur,
    },
  };
};
