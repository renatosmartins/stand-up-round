import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Input from "./Input";

describe("<Input>", () => {
  const onCommit = jest.fn();

  beforeEach(jest.clearAllMocks);

  it("renders", () => {
    render(<Input initialValue="abc" onCommit={onCommit} />);
    expect(screen.getByRole("textbox")).toHaveValue("abc");
  });

  it("triggers callback on enter", async () => {
    render(<Input initialValue="" onCommit={onCommit} />);
    const input = screen.getByRole("textbox");
    expect(input).toHaveValue("");
    fireEvent.change(input, { target: { value: "abc" } });
    expect(input).toHaveValue("abc");
    expect(onCommit).not.toHaveBeenCalled();
    userEvent.click(input);
    expect(input).toHaveFocus();
    fireEvent.keyDown(input, { key: "Enter" });
    await waitFor(() => {
      expect(input).not.toHaveFocus();
    });
    expect(onCommit).toHaveBeenCalledTimes(1);
    expect(onCommit).toHaveBeenCalledWith("abc");
  });

  it("triggers callback on blur", () => {
    render(<Input initialValue="" onCommit={onCommit} />);
    const input = screen.getByRole("textbox");
    expect(input).toHaveValue("");
    fireEvent.change(input, { target: { value: "abc" } });
    expect(input).toHaveValue("abc");
    expect(onCommit).not.toHaveBeenCalled();
    fireEvent.blur(input);
    expect(onCommit).toHaveBeenCalledTimes(1);
    expect(onCommit).toHaveBeenCalledWith("abc");
  });
});
