import styled from "styled-components";

export const StyledInputBase = styled.input({
  background: "none",
  border: "none",
  padding: "0.2rem",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  fontSize: "1rem",
  minWidth: "100%",
  borderRadius: 4,

  "&:hover, &:focus": {
    background: "rgba(255, 255, 255, 0.4)",
  },
});

export const StyledInput = styled(StyledInputBase)(({ width }) => ({ width }));

export const StyledSpanSizer = styled.span({
  position: "absolute",
  overflow: "hidden",
  textOverflow: "ellipsis",
  whiteSpace: "nowrap",
  opacity: 0,
  pointerEvents: "none",
});
