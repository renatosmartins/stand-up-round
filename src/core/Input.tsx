import { useInput } from "./Input.hook";
import { StyledInput, StyledSpanSizer } from "./Input.style";

export interface InputProps {
  initialValue: string;
  onCommit: (newContent: string) => void;
}

const Input = (props: InputProps) => {
  const { sizerProps, inputProps } = useInput(props);
  return (
    <>
      <StyledSpanSizer {...sizerProps} />
      <StyledInput {...inputProps} />
    </>
  );
};

export default Input;
