import { Person } from "../people";

export const allPeople = [
  { id: 1, name: "P1" },
  { id: 2, name: "P2" },
  { id: 3, name: "P3", hidden: true },
];
export const personWithAngles = (p: Person) => p;
export const savePeople = () => null;
export const getPersonInitials = () => "P";
