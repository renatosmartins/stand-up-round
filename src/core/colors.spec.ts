import { getColor } from "./colors";

describe("colors", () => {
  describe("getColor", () => {
    it("returns an opaque color", () => {
      expect(getColor(0)).toBe("hsl(0deg 100% 65%)");
      expect(getColor(20)).toBe("hsl(20deg 100% 65%)");
    });

    it("returns a transparent color", () => {
      expect(getColor(42, 0)).toBe("hsl(42deg 100% 65%)");
      expect(getColor(42, 0.5)).toBe("hsl(42deg 100% 65% / 50%)");
    });

    it("limits hue between 0 and 360", () => {
      expect(getColor(-20)).toBe("hsl(340deg 100% 65%)");
      expect(getColor(380)).toBe("hsl(20deg 100% 65%)");
    });

    it("limits transparency between 0 and 1", () => {
      expect(getColor(42, -0.1)).toBe("hsl(42deg 100% 65%)");
      expect(getColor(42, 1.1)).toBe("hsl(42deg 100% 65% / 100%)");
    });
  });
});
