export const startingColorHue = Math.random() * 360;

export const getColor = (hue: number, transparency: number = 0): string => {
  const angle = ((hue % 360) + 360) % 360;
  const transparencyPercentage = Math.min(1, Math.max(0, transparency)) * 100;

  return `hsl(${angle}deg 100% 65%${
    transparencyPercentage ? ` / ${transparencyPercentage}%` : ""
  })`;
};
