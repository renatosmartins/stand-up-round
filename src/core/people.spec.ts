import { getPersonInitials } from "./people";

describe("people", () => {
  describe("getPersonInitials", () => {
    it("converts a person's name into initials", () => {
      expect(getPersonInitials("")).toBe("");
      expect(getPersonInitials("123")).toBe("1");
      expect(getPersonInitials("abc")).toBe("A");
      expect(getPersonInitials("ábc")).toBe("Á");
      expect(getPersonInitials("firstname lastname")).toBe("FL");
      expect(getPersonInitials("firstname of lastname")).toBe("FL");
    });
  });
});
