import { startingColorHue } from "./colors";

export interface Person {
  id: number;
  name: string;
  avatar?: string;
  done?: boolean;
  hidden?: boolean;
}

export interface PersonWithAngles extends Person {
  hue: number;
  angle: number;
}

let counter = 0;

export const allPeople: Person[] = (
  JSON.parse(localStorage.getItem("people") || "[]") as Person[]
).map((p) => {
  p.id = ++counter;
  return p;
});

export const newPerson = (): Person => ({ id: ++counter, name: "" });

export const savePeople = (people: Person[]) => {
  localStorage.setItem(
    "people",
    JSON.stringify(people.map((p) => ({ ...p, id: undefined })))
  );
};

export const getPersonInitials = (name: string) =>
  name
    .split(" ")
    .map((s) => (s[0] || "").toUpperCase())
    .filter((_, i, names) => i === 0 || i === names.length - 1)
    .join("");

export const personWithAngles = (
  person: Person,
  index: number,
  people: Person[]
): PersonWithAngles => {
  const angle = (index * 360) / people.length;
  return {
    ...person,
    angle,
    hue: startingColorHue + angle,
  };
};
