import { useCallback, useEffect, useRef } from "react";
import { useAppContext } from "../core/AppContext";
import { newPerson } from "../core/people";

export const useManageDialog = () => {
  const { people, setPeople, isManageDialogVisible, setManageDialogVisible } =
    useAppContext();
  const gridRef = useRef<HTMLDivElement>(null);

  const addPerson = useCallback(() => {
    setPeople([...people, newPerson()]);
  }, [people, setPeople]);

  const onClose = useCallback(
    () => setManageDialogVisible(false),
    [setManageDialogVisible]
  );

  useEffect(() => {
    const nameInputs = Array.from<HTMLInputElement>(
      gridRef.current?.querySelectorAll(".person-name input") || []
    );
    const lastInput = nameInputs[nameInputs.length - 1];
    if (!lastInput) {
      return;
    }
    if (lastInput.value === "") {
      lastInput.focus();
    }
  }, [people]);

  return {
    people,
    dialogProps: {
      visible: isManageDialogVisible,
      onClose,
    },
    gridProps: {
      ref: gridRef,
    },
    addButtonProps: { onClick: addPerson },
  };
};
