import styled from "styled-components";

export const StyledGrid = styled.div({
  display: "grid",
  gridTemplateColumns: "repeat(4, auto)",
  marginBottom: "1rem",
});

export const StyledCell = styled.div({
  display: "flex",
  alignItems: "center",
  padding: "0.5rem",
});

export const StyledHeaderCell = styled(StyledCell)({
  fontSize: "1.5rem",
  fontWeight: "bold",
  justifyContent: "center",
});

export const StyledNewPerson = styled.div({
  display: "flex",
  justifyContent: "center",
});
