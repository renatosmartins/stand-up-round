import Dialog from "../core/Dialog";
import Button from "../core/Button";
import PersonRow from "./PersonRow";
import { useManageDialog } from "./ManageDialog.hook";
import {
  StyledGrid,
  StyledHeaderCell,
  StyledNewPerson,
} from "./ManageDialog.style";

const headers = [
  { label: "👁", title: "visible?" },
  { label: "👤", title: "name" },
  { label: "🔠", title: "initials" },
  { label: "", title: "actions" },
];

const ManageDialog = () => {
  const { people, dialogProps, gridProps, addButtonProps } = useManageDialog();
  return (
    <Dialog {...dialogProps}>
      {people.length > 0 && (
        <StyledGrid {...gridProps}>
          {headers.map((header) => (
            <StyledHeaderCell key={header.title} title={header.title}>
              {header.label}
            </StyledHeaderCell>
          ))}
          {people.map((p, i) => (
            <PersonRow key={`${i}${p.name}`} person={p} index={i} />
          ))}
        </StyledGrid>
      )}
      <StyledNewPerson>
        <Button {...addButtonProps} id="add-person" title="add person">
          ➕👤
        </Button>
      </StyledNewPerson>
    </Dialog>
  );
};

export default ManageDialog;
