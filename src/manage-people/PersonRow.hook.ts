import { useCallback } from "react";
import { useAppContext } from "../core/AppContext";
import { Person } from "../core/people";

export const usePersonRow = (person: Person) => {
  const { people, setPeople } = useAppContext();

  const updateHidden = useCallback(() => {
    person.hidden = !person.hidden;
    setPeople([...people]);
  }, [person, people, setPeople]);

  const updateName = useCallback(
    (newName: string) => {
      if (person.name === newName) {
        return;
      }
      person.name = newName;
      setPeople([...people]);
    },
    [person, people, setPeople]
  );

  const updateAvatar = useCallback(
    (imageUrl?: string) => {
      person.avatar = imageUrl;
      setPeople([...people]);
    },
    [person, people, setPeople]
  );

  const removePerson = useCallback(() => {
    setPeople(people.filter((p) => p !== person));
  }, [person, people, setPeople]);

  return {
    inputVisibleProps: {
      onClick: updateHidden,
    },
    inputNameProps: {
      initialValue: person.name,
      onCommit: updateName,
    },
    inputAvatarProps: {
      onImage: updateAvatar,
    },
    buttonRemoveProps: {
      onClick: removePerson,
    },
  };
};
