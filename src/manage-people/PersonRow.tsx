import AvatarInput from "../core/AvatarInput";
import ButtonSmall from "../core/ButtonSmall";
import Input from "../core/Input";
import { Person } from "../core/people";
import { StyledCell } from "./ManageDialog.style";
import { usePersonRow } from "./PersonRow.hook";

interface PersonRowProps {
  person: Person;
  index: number;
}

const PersonRow = ({ person, index }: PersonRowProps) => {
  const {
    inputVisibleProps,
    inputNameProps,
    inputAvatarProps,
    buttonRemoveProps,
  } = usePersonRow(person);
  return (
    <>
      <StyledCell id={`person-${index}-visible`}>
        <ButtonSmall {...inputVisibleProps}>
          {person.hidden ? " " : "✔︎"}
        </ButtonSmall>
      </StyledCell>
      <StyledCell id={`person-${index}-name`} className="person-name">
        <Input {...inputNameProps} />
      </StyledCell>
      <StyledCell id={`person-${index}-avatar`}>
        <AvatarInput person={person} index={index} {...inputAvatarProps} />
      </StyledCell>
      <StyledCell id={`person-${index}-remove`}>
        <ButtonSmall {...buttonRemoveProps} title="remove person">
          ✕
        </ButtonSmall>
      </StyledCell>
    </>
  );
};

export default PersonRow;
