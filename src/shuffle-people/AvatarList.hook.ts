import { useMemo } from "react";
import { useAppContext } from "../core/AppContext";

export const useAvatarList = () => {
  const { visiblePeople } = useAppContext();
  const avatarSize = useMemo(
    () =>
      visiblePeople.length > 10
        ? (Math.PI * 90 * 0.7) / visiblePeople.length
        : 20,
    [visiblePeople]
  );

  return { visiblePeople, avatarSize };
};
