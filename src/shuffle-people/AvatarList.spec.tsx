import { render } from "@testing-library/react";
import { useAppContext } from "../core/AppContext";
import Avatar from "../core/Avatar";
import AvatarList from "./AvatarList";

const people = [
  {
    id: 1,
    name: "Person Name 1",
    angle: 0,
    hue: 0,
    initials: "PN1",
  },
  {
    id: 2,
    name: "Person Name 2",
    angle: 0,
    hue: 0,
    initials: "PN2",
  },
];

jest.mock("../core/Avatar");
jest.mock("../core/AppContext");

const useAppContextMock = useAppContext as jest.MockedFunction<
  typeof useAppContext
>;

const MockedAvatar = (
  Avatar as jest.MockedFunction<typeof Avatar>
).mockImplementation(() => <></>);

describe("<AvatarList>", () => {
  beforeEach(jest.clearAllMocks);

  const setup = () => render(<AvatarList />);

  it("renders avatars", () => {
    useAppContextMock.mockReturnValue({
      people,
      setPeople: jest.fn(),
      visiblePeople: people,
      setVisiblePeople: jest.fn(),
      setSelectedPerson: jest.fn(),
      isManageDialogVisible: false,
      setManageDialogVisible: jest.fn(),
    });

    setup();
    expect(MockedAvatar).toHaveBeenCalledTimes(2);
    expect(MockedAvatar.mock.calls[0][0]).toMatchObject({
      person: people[0],
    });
    expect(MockedAvatar.mock.calls[1][0]).toMatchObject({
      person: people[1],
    });
  });
});
