import styled from "styled-components";
import Avatar from "../core/Avatar";

export const listSize = "90vmin";

export const StyledContainer = styled.div({
  flex: 1,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const StyledAvatarList = styled.div({
  display: "block",
  height: listSize,
  width: listSize,
  position: "relative",
  boxSizing: "border-box",
});

export const StyledAvatar = styled(Avatar)(({ person: { angle } }) => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: `translate(-50%, -50%) rotate(${angle}deg) translateY(calc((-${listSize} / 2) + 50%)) rotate(-${angle}deg)`,
  boxShadow: "0 4px 20px -10px black",
}));
