import { useAvatarList } from "./AvatarList.hook";
import {
  StyledAvatar,
  StyledAvatarList,
  StyledContainer,
} from "./AvatarList.style";
import Spinner from "./Spinner";

const AvatarList = () => {
  const { visiblePeople, avatarSize } = useAvatarList();
  return (
    <StyledContainer>
      <StyledAvatarList id="avatar-list" role="list">
        {visiblePeople.map((person, i) => (
          <StyledAvatar
            person={person}
            size={avatarSize}
            key={`${i}${person.angle}`}
          />
        ))}
        <Spinner />
      </StyledAvatarList>
    </StyledContainer>
  );
};

export default AvatarList;
