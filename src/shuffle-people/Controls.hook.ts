import { useCallback } from "react";
import { useAppContext } from "../core/AppContext";

export const useControls = () => {
  const { visiblePeople, setVisiblePeople, setManageDialogVisible } =
    useAppContext();

  const reset = useCallback(() => {
    setVisiblePeople(visiblePeople.map((p) => ({ ...p, done: false })));
  }, [visiblePeople, setVisiblePeople]);

  const manage = useCallback(() => {
    setManageDialogVisible(true);
  }, [setManageDialogVisible]);

  return { reset, manage };
};
