import { fireEvent, render, screen } from "@testing-library/react";
import { useAppContext } from "../core/AppContext";
import { Person, personWithAngles } from "../core/people";
import Controls from "./Controls";

const people: Person[] = [
  { id: 1, name: "A", done: true },
  { id: 2, name: "B", done: true },
  { id: 3, name: "C", done: false },
];

jest.mock("../core/AppContext");
const useAppContextMock = useAppContext as jest.MockedFunction<
  typeof useAppContext
>;

describe("<Controls>", () => {
  const setVisiblePeople = jest.fn();
  const setManageDialogVisible = jest.fn();
  const setSelectedPerson = jest.fn();

  beforeEach(jest.clearAllMocks);

  it("resets people state", () => {
    useAppContextMock.mockReturnValue({
      people,
      setPeople: jest.fn(),
      visiblePeople: people.map(personWithAngles),
      setVisiblePeople,
      setSelectedPerson,
      isManageDialogVisible: false,
      setManageDialogVisible,
    });

    render(<Controls />);
    expect(setVisiblePeople).toHaveBeenCalledTimes(0);
    fireEvent.click(screen.getByRole("button", { name: "reset" }));
    expect(setVisiblePeople).toHaveBeenCalledTimes(1);
    expect(setVisiblePeople.mock.calls[0][0]).toMatchObject([
      { done: false },
      { done: false },
      { done: false },
    ]);
  });

  it("opens modal to manage people", () => {
    useAppContextMock.mockReturnValue({
      people,
      setPeople: jest.fn(),
      visiblePeople: [],
      setVisiblePeople,
      setSelectedPerson,
      isManageDialogVisible: false,
      setManageDialogVisible,
    });

    render(<Controls />);
    expect(setManageDialogVisible).toHaveBeenCalledTimes(0);
    fireEvent.click(screen.getByRole("button", { name: "manage" }));
    expect(setManageDialogVisible).toHaveBeenCalledTimes(1);
    expect(setManageDialogVisible).toHaveBeenLastCalledWith(true);
  });
});
