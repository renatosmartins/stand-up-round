import styled from "styled-components";

export const StyledControls = styled.div({
  position: "absolute",
  display: "flex",
  gap: "8px",
  margin: "8px",
});
