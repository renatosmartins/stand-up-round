import Button from "../core/Button";
import { useControls } from "./Controls.hook";
import { StyledControls } from "./Controls.style";

const Controls = () => {
  const { reset, manage } = useControls();
  return (
    <StyledControls>
      <Button id="reset" aria-label="reset" onClick={reset}>
        ↺
      </Button>
      <Button id="manage" aria-label="manage" onClick={manage}>
        👥
      </Button>
    </StyledControls>
  );
};

export default Controls;
