import {
  CSSProperties,
  KeyboardEvent,
  useCallback,
  useMemo,
  useRef,
  useState,
} from "react";
import { useAppContext } from "../core/AppContext";
import spinToAngle from "./spinToAngle";

export const useSpinner = () => {
  const {
    visiblePeople,
    setVisiblePeople,
    setSelectedPerson,
    isManageDialogVisible,
  } = useAppContext();
  const possiblePeople = useMemo(
    () => visiblePeople.filter((p) => !p.done),
    [visiblePeople]
  );
  const isDisabled = useMemo(
    () => !possiblePeople.length || isManageDialogVisible,
    [possiblePeople, isManageDialogVisible]
  );
  const [angle, setAngle] = useState(0);
  const enabled = useRef<boolean>(true);
  const containerRef = useRef<HTMLDivElement>(null);
  const spinnerRef = useRef<HTMLDivElement>(null);

  const startSpinning = useCallback(async () => {
    if (!enabled.current) {
      return;
    }

    if (!spinnerRef.current || isDisabled) {
      return;
    }
    enabled.current = false;

    const randomIndex = Math.floor(Math.random() * possiblePeople.length);
    const randomPerson = possiblePeople[randomIndex];

    await spinToAngle(spinnerRef.current, randomPerson.angle);

    // this will reset the angle to a number between 0 and 360, but same position
    setAngle(randomPerson.angle);
    randomPerson.done = true;
    setSelectedPerson(randomPerson);
    setVisiblePeople([...visiblePeople]);
    enabled.current = true;
  }, [
    isDisabled,
    possiblePeople,
    setSelectedPerson,
    visiblePeople,
    setVisiblePeople,
  ]);

  const handleKeyDown = useCallback(
    (e: KeyboardEvent) => {
      if (!enabled.current) {
        return;
      }

      if (e.key === "Enter" || e.key === " ") {
        startSpinning();
        containerRef.current?.blur();
      }
    },
    [startSpinning]
  );

  return {
    containerProps: {
      ref: containerRef,
      onClick: startSpinning,
      onKeyDown: handleKeyDown,
      isDisabled,
      style: { "--angle": `${angle}deg` } as CSSProperties,
    },
    spinnerProps: {
      ref: spinnerRef,
    },
  };
};
