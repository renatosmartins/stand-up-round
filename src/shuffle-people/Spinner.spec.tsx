import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import cloneDeep from "lodash.clonedeep";
import { useAppContext } from "../core/AppContext";
import { PersonWithAngles } from "../core/people";
import Spinner from "./Spinner";
import spinToAngle, { minimumSpins } from "./spinToAngle";

const people: PersonWithAngles[] = [
  { id: 1, name: "A", hue: 0, angle: 0 },
  { id: 2, name: "B", hue: (1 / 3) * 360, angle: (1 / 3) * 360 },
  { id: 3, name: "C", hue: (2 / 3) * 360, angle: (2 / 3) * 360 },
];

const originalMath = global.Math;
const mockMath = Object.create(global.Math);

jest.mock("./spinToAngle");
const spinToAngleMock = spinToAngle as jest.MockedFunction<typeof spinToAngle>;
spinToAngleMock.mockImplementation((el: HTMLElement, angle: number) => {
  el.setAttribute(
    "style",
    `transform: rotate(${angle + minimumSpins * 360}deg)`
  );
  return Promise.resolve();
});

jest.mock("../core/AppContext");
const useAppContextMock = useAppContext as jest.MockedFunction<
  typeof useAppContext
>;

describe("<Spinner>", () => {
  const setVisiblePeople = jest.fn();
  const setSelectedPerson = jest.fn();

  beforeEach(() => {
    mockMath.random = jest.fn(() => 2 / people.length);
    global.Math = mockMath;
    const clonedPeople = cloneDeep(people);
    useAppContextMock.mockReturnValue({
      people: clonedPeople,
      setPeople: jest.fn(),
      visiblePeople: clonedPeople,
      setVisiblePeople,
      setSelectedPerson,
      isManageDialogVisible: false,
      setManageDialogVisible: jest.fn(),
    });
  });
  beforeEach(jest.clearAllMocks);

  afterEach(() => {
    global.Math = originalMath;
  });

  it("selects a person on click", async () => {
    render(<Spinner />);
    expect(setVisiblePeople).toHaveBeenCalledTimes(0);
    expect(setSelectedPerson).toHaveBeenCalledTimes(0);
    fireEvent.click(screen.getByRole("button"));
    await waitFor(() => {
      expect(setVisiblePeople).toHaveBeenCalledTimes(1);
    });
    expect(setVisiblePeople.mock.calls[0][0]).toMatchObject([
      { name: "A" },
      { name: "B" },
      { name: "C", done: true },
    ]);
    expect(setSelectedPerson).toHaveBeenCalledTimes(1);
    expect(setSelectedPerson.mock.calls[0][0]).toMatchObject({
      name: "C",
      done: true,
    });
  });

  it("selects a person on Enter key pressed", async () => {
    render(<Spinner />);
    expect(setVisiblePeople).toHaveBeenCalledTimes(0);
    expect(setSelectedPerson).toHaveBeenCalledTimes(0);
    fireEvent.keyDown(screen.getByRole("button"), { key: "Enter" });
    await waitFor(() => {
      expect(setVisiblePeople).toHaveBeenCalledTimes(1);
    });
    expect(setVisiblePeople.mock.calls[0][0]).toMatchObject([
      { name: "A" },
      { name: "B" },
      { name: "C", done: true },
    ]);
    expect(setSelectedPerson).toHaveBeenCalledTimes(1);
    expect(setSelectedPerson.mock.calls[0][0]).toMatchObject({
      name: "C",
      done: true,
    });
  });

  it("selects a person on Space key pressed", async () => {
    render(<Spinner />);
    expect(setVisiblePeople).toHaveBeenCalledTimes(0);
    expect(setSelectedPerson).toHaveBeenCalledTimes(0);
    fireEvent.keyDown(screen.getByRole("button"), { key: " " });
    await waitFor(() => {
      expect(setVisiblePeople).toHaveBeenCalledTimes(1);
    });
    expect(setVisiblePeople.mock.calls[0][0]).toMatchObject([
      { name: "A" },
      { name: "B" },
      { name: "C", done: true },
    ]);
    expect(setSelectedPerson).toHaveBeenCalledTimes(1);
    expect(setSelectedPerson.mock.calls[0][0]).toMatchObject({
      name: "C",
      done: true,
    });
  });
});
