import styled from "styled-components";

const arrowHeight = 24;
const arrowWidth = 3;

const StyledContainerBase = styled.div({
  position: "absolute",
  left: "50%",
  top: "50%",
  padding: "0 20vmin",
  height: "auto",
  width: "20vmin",
  aspectRatio: "1",
  borderRadius: "50%",
  boxSizing: "border-box",
  transform: "translate(-50%, -50%)",
  outline: "none",
});

export const StyledContainer = styled(StyledContainerBase)<{
  isDisabled: boolean;
}>(({ isDisabled }) =>
  isDisabled ? { display: "none" } : { cursor: "pointer" }
);

export const StyledRadial = styled.div({
  position: "absolute",
  left: "50%",
  top: "50%",
  height: "70%",
  width: "40%",
  borderRadius: "50%",
  transform: "translate(-50%, -50%) rotate(var(--angle))",

  [`${StyledContainer}:focus-visible &`]: {
    background: "radial-gradient(#1e90ff78 10%, transparent 70%)",
  },
});

export const StyledArrow = styled.div({
  position: "absolute",
  left: "50%",
  top: "50%",
  height: `${arrowHeight}vmin`,
  width: `${arrowWidth}vmin`,
  marginTop: `-${arrowHeight / 2}vmin`,
  marginLeft: `-${arrowWidth / 2}vmin`,
  background: "#32261b",
  borderRadius: "5px",
  transform: "rotate(var(--angle))",

  "&::before": {
    top: 0,
    left: 0,
    display: "block",
    border: "5vmin solid transparent",
    borderBottom: "8vmin solid #32261b",
    borderRadius: "10px",
    boxSizing: "border-box",
    transform: `translate(calc(-50% + ${arrowWidth / 2}vmin), -80%)`,
    content: "''",
  },
});
