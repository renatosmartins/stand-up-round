import { useSpinner } from "./Spinner.hook";
import { StyledContainer, StyledRadial, StyledArrow } from "./Spinner.style";

const Spinner = () => {
  const { containerProps, spinnerProps } = useSpinner();
  return (
    <StyledContainer id="spinner-container" tabIndex={0} {...containerProps}>
      <StyledRadial />
      <StyledArrow id="spinner" role="button" {...spinnerProps} />
    </StyledContainer>
  );
};

export default Spinner;
