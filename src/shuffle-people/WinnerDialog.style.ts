import styled from "styled-components";

export const StyledWinner = styled.div({
  fontSize: "2rem",
  fontWeight: 600,
  textAlign: "center",
  textShadow: "1px 1px 3px rgba(0, 0, 0, 0.2)",
});
