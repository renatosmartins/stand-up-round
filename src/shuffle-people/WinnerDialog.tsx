import { useAppContext } from "../core/AppContext";
import Dialog from "../core/Dialog";
import { StyledWinner } from "./WinnerDialog.style";

const WinnerDialog = () => {
  const { selectedPerson, setSelectedPerson } = useAppContext();
  return selectedPerson ? (
    <Dialog
      visible={Boolean(selectedPerson)}
      onClose={() => setSelectedPerson(undefined)}
    >
      <StyledWinner id="winner">{selectedPerson.name}!</StyledWinner>
    </Dialog>
  ) : null;
};

export default WinnerDialog;
