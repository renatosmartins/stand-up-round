import AvatarList from "./AvatarList";
import WinnerDialog from "./WinnerDialog";

const ShufflePeople = () => (
  <>
    <AvatarList />
    <WinnerDialog />
  </>
);

export default ShufflePeople;
