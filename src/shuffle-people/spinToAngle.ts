export const minimumSpins = 6;
export const spinningDuration = 1000;

const spinToAngle = async (el: HTMLElement, angle: number) => {
  const anglePlusSpins = angle + minimumSpins * 360;
  const animation = el.animate(
    [
      // split the transformation into multiple steps to force it to at least get some spins,
      // otherwise sometimes it might not even do a full turn.
      { transform: `rotate(${anglePlusSpins / 3}deg)` },
      { transform: `rotate(${anglePlusSpins / (3 / 2)}deg)` },
      { transform: `rotate(${anglePlusSpins}deg)` },
    ],
    {
      duration: spinningDuration,
      easing: "cubic-bezier(0, 0.5, 0.2, 1)",
      fill: "forwards",
    }
  );

  await animation.finished;
};

export default spinToAngle;
