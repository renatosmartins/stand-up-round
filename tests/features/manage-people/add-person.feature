Feature: Add person

  I want to add a new person to the list

  @provision-none
  Scenario: Auto open management dialog
    Given there are no people
    Then the management dialog should be open

  @provision-all
  Scenario: Add new person
    When I open the management dialog
    And I fill the person form
    Then a new avatar should appear on the circle
