Feature: Hide person

  I want to temporarily disable a person, so that they are not selected on the current stand up.

  @provision-all
  Scenario: Hide a person
    When I open the management dialog
    And I uncheck the visibility checkbox of a person
    Then the avatar list does not contain that person

  @provision-hidden
  Scenario: Show a hidden person
    When I open the management dialog
    And I check the visibility checkbox of the hidden person
    Then the avatar list contains that person

  @provision-selected
  Scenario: Hiding a person does not reset the current round
    When I open the management dialog
    And the person "P3" (of total 4 people) is selected
    And I uncheck the visibility checkbox of a person
    Then the person "P3" (of total 3 people) is selected
