Feature: Remove person

  I want to remove a person from the list.

  @provision-all
  Scenario: Remove person
    When I open the management dialog
    And I click on the remove button of a person
    Then the person is removed from the list
    And the avatar list does not contain that person
