Feature: Reset

  I want to reset the stand up round so I can run it again.

  @provision-selected
  Scenario: Reset
    When I click the reset button
    Then all avatars are opaque
