Feature: Select random person

  I want to be able to select a random person for the stand up round

  @provision-all
  Scenario: Select random person
    When I click on the spinner arrow
    Then a dialog pops up with a person name
    And the spinner arrow is pointing to the selected person
    And the selected person's avatar is semi-transparent
