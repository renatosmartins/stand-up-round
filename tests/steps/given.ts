import { Given } from "@cucumber/cucumber";
import { expect } from "@playwright/test";
import { expectOpacities } from "./then";

Given("there are no people", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars).toStrictEqual([]);
});

Given(
  "the person {string} \\(of total {int} people) is selected",
  async (personInitials: string, expectedLength: number) => {
    const personIndex = (
      await Promise.all(
        (
          await global.page.$$("#avatar-list [role=figure]")
        ).map((e) => e.textContent())
      )
    ).findIndex((text) => text === personInitials);
    await expectOpacities(expectedLength, (i) =>
      i === personIndex ? "0.4" : "1"
    );
  }
);
