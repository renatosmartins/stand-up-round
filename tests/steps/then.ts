import { Then } from "@cucumber/cucumber";
import { expect } from "@playwright/test";
import { expectedPerson } from "../support/data";

export const expectOpacities = async (
  expectedLength: number,
  matcherFn: (index: number) => string
) => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  const opacities = await Promise.all(
    avatars.map(
      async (avatar) =>
        await avatar.evaluate((el) => getComputedStyle(el).opacity)
    )
  );
  expect(opacities.length).toBe(expectedLength);
  opacities.forEach((opacity, index) => expect(opacity).toBe(matcherFn(index)));
};

Then("a dialog pops up with a person name", async () => {
  const dialog = global.page.locator("#winner");
  expect(await dialog.isVisible()).toBeTruthy();
  expect(await dialog.textContent()).toBe(`${expectedPerson.name}!`);
});

Then("the spinner arrow is pointing to the selected person", async () => {
  const spinner = await global.page.$("#spinner");
  expect(spinner).toBeDefined();
  const angle = await spinner?.evaluate((el) =>
    parseFloat(getComputedStyle(el).getPropertyValue("--angle"))
  );
  expect(angle).toBeCloseTo(expectedPerson.angle);
});

Then("the selected person's avatar is semi-transparent", async () => {
  await expectOpacities(4, (i) => (i === expectedPerson.index ? "0.4" : "1"));
});

Then("all avatars are opaque", async () => {
  await expectOpacities(4, () => "1");
});

Then("the management dialog should be open", async () => {
  expect(await global.page.locator("#add-person").isVisible()).toBeTruthy();
});

Then("a new avatar should appear on the circle", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars.length).toBe(5);
  expect(await avatars[avatars.length - 1].textContent()).toBe("FL");
  expect(await avatars[avatars.length - 1].getAttribute("title")).toBe(
    "Firstname Lastname"
  );
});

// after hiding the first person
Then("the avatar list does not contain that person", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars.length).toBe(3);
  expect(await avatars[0].textContent()).toBe("P1");
  expect(await avatars[0].getAttribute("title")).toBe("Person 1");
});

Then("the avatar list contains that person", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars.length).toBe(4);
  expect(await avatars[0].textContent()).toBe("P0");
  expect(await avatars[0].getAttribute("title")).toBe("Person 0");
});

Then("the person is removed from the list", async () => {
  const people = global.page.locator(".person-name");
  expect(await people.count()).toBe(3);
  expect(await people.first().textContent()).toBe("Person 1");
});
