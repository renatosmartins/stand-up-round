import { When } from "@cucumber/cucumber";
import { expect } from "@playwright/test";

When("I click on the spinner arrow", async () => {
  await global.page.click("#spinner-container");
});

When("I click the reset button", async () => {
  await global.page.click("#reset");
});

When("I open the management dialog", async () => {
  await global.page.click("#manage");
  expect(await global.page.locator("#add-person").isVisible()).toBeTruthy();
});

When("I fill the person form", async () => {
  const people = global.page.locator(".person-name");
  expect(await people.count()).toBe(4);
  await global.page.locator("#add-person").click();
  expect(await people.count()).toBe(5);
  expect(await global.page.locator("#person-4-visible").textContent()).toBe(
    "✔︎"
  );
  const nameInput = global.page.locator("#person-4-name input");
  expect(await nameInput.inputValue()).toBe("");
  await nameInput.type("Firstname Lastname");
  await nameInput.press("Enter");
  expect(await nameInput.inputValue()).toBe("Firstname Lastname");
  expect(await global.page.locator("#person-4-avatar").textContent()).toBe(
    "FL"
  );
});

// makes the first person invisible
When("I uncheck the visibility checkbox of a person", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars.length).toBe(4);

  const visibleCheckbox = global.page.locator("#person-0-visible");
  expect(await visibleCheckbox.textContent()).toBe("✔︎");
  await visibleCheckbox.click();
  expect(await visibleCheckbox.textContent()).toBe(" ");
});

// makes the first person visible
When("I check the visibility checkbox of the hidden person", async () => {
  const avatars = await global.page.$$("#avatar-list [role=figure]");
  expect(avatars.length).toBe(3);

  const visibleCheckbox = global.page.locator("#person-0-visible");
  expect(await visibleCheckbox.textContent()).toBe(" ");
  await visibleCheckbox.click();
  expect(await visibleCheckbox.textContent()).toBe("✔︎");
});

When("I click on the remove button of a person", async () => {
  const people = global.page.locator(".person-name");
  expect(await people.count()).toBe(4);
  expect(await people.first().textContent()).toBe("Person 0");
  await global.page.locator("#person-0-remove").click();
});
