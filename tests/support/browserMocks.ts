import { expectedNumberOfPeople, expectedPerson } from "./data";

const browserMocks = `
  // set random to always return a given number to corresponds to the expected person
  Math.random = () => ${expectedPerson.index / expectedNumberOfPeople};

  // disable element animations by jumping directly to the finished state
  const originalAnimate = Element.prototype.animate;
  Element.prototype.animate = function animate(...args) {
    const animation = originalAnimate.apply(this, args);
    animation.finish();
    return animation;
  };
`;

export default browserMocks;
