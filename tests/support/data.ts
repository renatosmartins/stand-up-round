export const initialPeople = [
  { name: "Person 0", initials: "P0" },
  { name: "Person 1", initials: "P1" },
  { name: "Person 2", initials: "P2" },
  { name: "Person 3", initials: "P3" },
];

export const initialPeopleWithOneSelected = [
  { name: "Person 0", initials: "P0" },
  { name: "Person 1", initials: "P1" },
  { name: "Person 2", initials: "P2" },
  { name: "Person 3", initials: "P3", done: true },
];

export const initialPeopleWithOneHidden = [
  { name: "Person 0", initials: "P0", hidden: true },
  { name: "Person 1", initials: "P1" },
  { name: "Person 2", initials: "P2" },
  { name: "Person 3", initials: "P3" },
];

export const expectedNumberOfPeople = 4;

export const expectedPerson = {
  index: 2,
  name: "Person 2",
  angle: (2 / expectedNumberOfPeople) * 360,
};
