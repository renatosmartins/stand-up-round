import { After, AfterAll, Before, BeforeAll } from "@cucumber/cucumber";
import {
  BrowserContext,
  chromium,
  ChromiumBrowser,
  FirefoxBrowser,
  Page,
  WebKitBrowser,
} from "@playwright/test";
import browserMocks from "./browserMocks";
import { type Person } from "../../src/core/people";
import {
  initialPeople,
  initialPeopleWithOneHidden,
  initialPeopleWithOneSelected,
} from "./data";

declare global {
  var browser: ChromiumBrowser | FirefoxBrowser | WebKitBrowser;
  var context: BrowserContext;
  var page: Page;
}

type PeopleProvisioner = (p: Person[]) => void;

BeforeAll(async () => {
  global.browser = await chromium.launch({ headless: true });
});

AfterAll(async () => {
  await global.browser.close();
});

Before(async () => {
  global.context = await global.browser.newContext();
  global.page = await global.context.newPage();
  await global.context.addInitScript({ content: browserMocks });
});

Before({ tags: "@provision-none" }, async () => {
  await global.context.addInitScript((() => {
    localStorage.setItem("people", JSON.stringify([]));
  }) as PeopleProvisioner);
});

Before({ tags: "@provision-all" }, async () => {
  await global.context.addInitScript(
    ((people) => {
      localStorage.setItem("people", JSON.stringify(people));
    }) as PeopleProvisioner,
    initialPeople
  );
});

Before({ tags: "@provision-hidden" }, async () => {
  await global.context.addInitScript(
    ((people) => {
      localStorage.setItem("people", JSON.stringify(people));
    }) as PeopleProvisioner,
    initialPeopleWithOneHidden
  );
});

Before({ tags: "@provision-selected" }, async () => {
  await global.context.addInitScript(
    ((people) => {
      localStorage.setItem("people", JSON.stringify(people));
    }) as PeopleProvisioner,
    initialPeopleWithOneSelected
  );
});

Before(async () => {
  await global.page.goto("http://localhost:3000");
});

After(async () => {
  await global.page.close();
});
